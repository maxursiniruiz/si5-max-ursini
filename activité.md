Max URSINI  
SIO SLAM 1  
Janvier 2019

**Activité 1** :
=
**Etape 1** : Installer VirtualBox, logiciel d’hypervision
-

Pour installer VirtualBox:

- Cliquer sur ce lien pour télécharger VirtualBox : <https://www.virtualbox.org/wiki/Downloads> 

- Séléctionner l'OS en fonction du système d'exploitation d'hôte, Windows 10.

- Si la virtualisation est impossible, accéder au BIOS en restant appuyé sur la touche "shift" en arrêtant l'ordinateur, puis rallumer l'ordinateur en restant appuyré sur la touche "F2".  

- C'est une virtualisation totale.

**Etape 2** : Création d'une machine
-

Créer une première machine virtuelle avec un système d’exploitation Linux Debian.

- Télécharger _Debian_ en cliquant sur ce lien : <https://www.debian.org/distrib/>, choisir "_image ISO "netinst" pour PC 64 bits_"

- Ouvrir VirtualBox  


- Créer une nouvelle machine virtuelle ; donner un nom à cette machine, de type "_linux_" et de version "_Linux 2.6 / 3.x / 4.x (64 bits)_" 

- Taille de mémoire de 1024.

- Pour le disque dur cocher "_Créer un disque dur virtuel maintenant_"

- Pour les types de fichiers du disque dur cocher "_VDI (Image de disque VirtualBox)_"

- Pour le stockage sur disque dur physique cocher "_Dynamiquement alloué_"

- Emplacement et taille de 8Gio

- Finalement, démarrer la machine

- Se connecter !

- Choisir le disque de démarrage Debian téléchargé au préalable  

- Continuer sans ne rien indiquer jusqu'à la création d'un nouvel utilisateur, entrer alors un identifiant et un mot de passe ( pas trop compliqué :D )

- cocher "_oui_" pour appliquer les changements

- Choisir "_ftp.fr.debian.org"

- Au moment de la selection des logiciels il faut séléctionner au minimum le "_serveur SSH_" et "_GNOME_".

**Etape 3** : Configurer le réseau de sorte que la machine puisse avoir accès au réseau internet accessible par la machine
hôte.
-


- Pour permettre à la machine virtuelle d'accéder à Internet :  
Go dans "_configuration_" dans VirtualBox, puis dans l'onglet "_réseau_"  

- Pour vérifier l'accès à Internet par la machine virtuelle :  
Essayer d'aller sur Internet avec Firefox



- Cela ne permet pas à la machine hôte d'accéder à la machine virtuelle.  



- Pour vérifier l'accès à la machine virtuelle par la machine hôte :  
Démarrer la machine virtuelle, accéder au terminal puis faire "_ip a_" pour avoir l'adresse IP de la machine virtuelle

invite de commande (cmd) >>>>> "_ping -t 10.0.2.15_"


**Etape 4** : Configurer la machine virtuelle de sorte qu’elle puisse avoir accès à un dossier partagé rendu disponible par la
machine hôte.
-

- Cliquer sur périphériques puis "Insérer l'image CD des Additions invité"
- Lancer le logiciel

- Une clé est nécéssaire, c'est le mot de passe de la session
- Le logiciel se lance et s'installe.

- Retourner dans "Périphériques" > Lecteurs CD/DVD > Ejecter le disque du lecteur virtuel
- Redémarrer la machine Virtuel linux

- Ensuite créer un dossier partagé en configuration permanente.

- Ouvrir le terminal et lancer cette commande :
mount -t vboxsf partage-virtual /home/fafa/partage (par défaut par rapport au dossier)

Vous pouvez désormais aller dans le dossier « partage » (ou le nom que vous lui avez donné), vous pouvez désormais envoyer des fichiers par ce canal sur l’une ou l’autre machine.

**Etape 5** : Configurer le réseau de sorte que la machine hôte puisse discuter avec sa machine virtuelle et vice-versa
-

- Il faut se rendre dans les paramètres de la machine virtuelle et modifier les options.

- Cliquez sur le "+" , cela permettra de créer une interface réseau.

- Attaché à > Adaptateur réseau hote > VALIDEZ.

- Selectionnez les deux connexions virtuelles (réseau local et celle crée précédemment.

- Clique droit > Connexion de ponts.



