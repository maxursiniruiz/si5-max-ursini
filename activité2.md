# Etape n°1 :

<ul>
<li>Fichier sources.list
<br>cat /etc/aptsources.list 
<br>Version 9.7 , l'ordre d'éxécution n'est pas significatif.

<br><li><strong> Mise à jour de la liste des packets courants : </strong>
<br>sudo apt-get update</li>

<li><strong> Mise à jour de tous les packets installé:</strong>
<br>sudo apt-get dist-upgrade


# Etape n°2 :

<li><strong> Comment savoir si APACHE est bien installé :</strong>
<br>dpkg -l 
<br>grep apache2

<br><li><strong>Comment vérifier si APACHE fonctionne : </strong>
<br>Demander "localhost" sur le navigateur  La page web devrais afficher "It works".

<br><li><strong>Comment chercher si un packet Debian est déjà installé: </strong>
<br>dpkg -l "package_name"

<br><li><strong>Comment installer un packet Debian : </strong>
sudo apt-get install "package_name"

<br><li><strong> Commande pour stopper APACHE: </strong>
sudo systemctl stop apache2 
<br><li><strong>Commande pour lancer APACHE : </strong>
sudo systemctl start apache2 
<br><li><strong>Commande pour relancer APACHE : </strong>
sudo systemctl restart apache2

# Etape n°3 :

<br><li><strong>Comment chercher le groupe www-data: </strong>
<br>cat /etc/group

<br><li><strong>Comment trouver le propriétaire des fichiers dans var/www: </strong>
<br>ls -l 

<br><li><strong>Comment donner les droits d'accès au groupe www-data pour voir les fichiers </strong>
<br>de var/www: 
<br>chown -R www-data: *

<br><li><strong> Trouver les doits du groupe www-data sur les fichiers de var/www: </strong>
<br>ls -l 
<br>le groupe www-data a les droits d'écriture

# Etape n°4 :

<br><li><strong>Commande pour créer un nouvel utilisateur: </strong>
<br>sudo useradd lucky -g www-data -d /home/lucky -m 
<br>sudo passwd lucky

<br><li><strong> Lucky a des droits de lecture </strong>
<br>sudo touch mon_fichier_texte.txt 
<br>Impossible d'ajouter un nouveau fichier.

<br><li><strong>Comment Modifier les droits de www-data pour lui permettre d'écrire </strong>
<br>sudo chown -R lucky:lucky /var/www

<br><li><strong> Comment créer un fichier HTML et ajout de texte: </strong>
<br>echo Fichier >> touch moi.html

# Etape n°5 :

<li><strong>Comment donner les droits de lecture et d'exécution : </strong>
<br>mkdir ~/public_html 
<br>echo 'Mon site personnel' > ~/public_html/moi.html 
<br>chmod -R 755 ~/public_html

<br><li><strong>Comment activer le module mod_userdir : </strong>
<br>sudo a2enmod userdir 
<br>sudo systemctl reload apache2

# Etape n°6 :

<li><strong>PSTREE va permettre d'afficher l'arborescences des processus. </strong>
<br>Le processus APACHE est allumé

<br><li><strong> Et ensuite:</strong>
<br> sudo killall apache2
<br> PSTREE et APACHE disparaissent donc.